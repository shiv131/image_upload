const ejs = require('ejs');
const path = require('path');
var fs = require('fs');

var express = require('express'),
    aws = require('aws-sdk'),
    bodyParser = require('body-parser'),
    multer = require('multer'),
    multerS3 = require('multer-s3');


 //######## upload image on AWS #######//  

var app = express(),
        s3Config = new aws.S3({
    	secretAccessKey: 'bOB+pAqpwIlhw/2FWHdowZaRn5WrJv2uuq4ZHKRZ',
    	accessKeyId: 'AKIAI3EAE5T7FKEAM6JA'
     });

app.use(bodyParser.json());

var upload = multer({

    storage: multerS3({
	    s3: s3Config,
	     bucket: 'brainmobi',
	     key: function (req, file, cb) {
	        console.log(file);
	        cb(null, file.originalname); 
	        }
      })
 });

app.post('/upload', upload.array('myImage'), function (req, res, next) {
    res.send("Your Image is Successfully Uploaded!");
   });


//******* End upload image on AWS ********//


    // EJS setup


app.set('view engine', 'ejs');

app.use(express.static('./public'));

app.get('/', (req, res) => res.render('index'));

app.post('/upload', (req, res) => {
	upload(req, res, (err) => {
		if(err){
		  res.render('index', {
		   msg: err
		   });

		  } else {
		    // console.log(req.file);
		    if(req.file == undefined){
				res.render('index', {
				msg: 'Error: File not Selected! please select!'
				});
		      } else {
			      res.render('index', {
				  msg: 'File successfully Uploaded!',
				  file: `uploads/${req.file.filename}`
			      });
		       }
		   }
	 });
});


//##### Delete uploaded file from AWS #######//

	var deleteParam = {
		Bucket: 'brainmobi',
		Delete: {
		Objects: [{Key: 'bg.jpg'},]
		        }
		}; 
		s3Config.deleteObjects(deleteParam, function(err, data) {
		if (err) console.log(err, err.stack);
		else console.log('delete', data);
	  });

//End of delete uploaded file from AWS


const port = 3002;

app.listen(port, () => console.log(`Server start on port: ${port}`));

