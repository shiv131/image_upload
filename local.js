const ejs = require('ejs');
const path = require('path');
var fs = require('fs');

var express = require('express'),
    aws = require('aws-sdk'),
    bodyParser = require('body-parser'),
    multer = require('multer'),
    multerS3 = require('multer-s3');


//###### upload on local server ######//

	//Set The Storage Engine
		const storage = multer.diskStorage({
		destination: './public/uploads/',
		filename: function(req, file, cb){
		cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
		}
		});

	// Init Upload
		const upload = multer({
		storage: storage,
		limits:{fileSize: 1000000},
		fileFilter: function(req, file, cb){
		checkFileType(file, cb);
		}
		}).single('myImage');

	// Check File Type
		function checkFileType(file, cb){
		// Allowed ext
		const filetypes = /jpeg|jpg|png|gif/;
		// Check ext
		const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
		// Check mime
		const mimetype = filetypes.test(file.mimetype);

		if(mimetype && extname){
		return cb(null,true);
		} else {
		cb('Error: Please Choose Images Only!');
		}
		}

//******* End of upload on local server*********//


    // EJS setup

var app = express(); 

app.set('view engine', 'ejs');

app.use(express.static('./public'));

app.get('/', (req, res) => res.render('index'));

app.post('/upload', (req, res) => {
	upload(req, res, (err) => {
		if(err){
		  res.render('index', {
		   msg: err
		   });

		  } else {
		    // console.log(req.file);
		    if(req.file == undefined){
				res.render('index', {
				msg: 'Error: File not Selected! please select!'
				});
		      } else {
			      res.render('index', {
				  msg: 'File successfully Uploaded!',
				  file: `uploads/${req.file.filename}`
			      });
		       }
		   }
	 });
});


const port = 3005;

app.listen(port, () => console.log(`Server start on port: ${port}`));
